<?php

namespace Sda\Millionaires\Controller;

use Sda\Millionaires\Config\Config;
use Sda\Millionaires\Question\Question;
use Sda\Millionaires\Question\QuestionFactory;
use Sda\Millionaires\Request\Request;
use Sda\Millionaires\Session\Session;
use Sda\Millionaires\View\Template;

class Millionaires
{
    /**
     * @var array
     */
    private $templateParameters = [];
    /**
     * @var Template
     */
    private $templateEngine;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Question
     */
    private $question;

    public function __construct(
        Template $templateEngine,
        Request $request,
        Session $session
    )
    {
        $this->templateEngine = $templateEngine;
        $this->session = $session;
        $this->request = $request;
    }

    public function run()
    {
        $action = $this->request->getParamFromGet('action', 'rules');

        $username = $this->session->getFromSession('username');

        if ('' === $username){
            $username = $this->request->getParamFromPost('username', '');
        }

        if ('' === $username && 'login' !== $action) {
            header('Location: index.php?action=login');
        }

        $this->session->saveToSession('username', $username);

        $this->templateParameters['username'] = $username;

        switch ($action){
            case 'logout':
                $this->session->sessionDestroy();
                header('Location: index.php?action=login');
                break;
            case 'login':
                $templateName = 'login.html';
                break;
            case 'rules':
                $templateName = 'rules.html';
                break;
            case 'game':
                $templateName = 'game.html';
                $this->prepareGame();
                break;
            default:
                $templateName = 'error404.html';
                break;
        }


        $this->templateEngine->renderTemplate($templateName, $this->templateParameters);
    }

    private function prepareGame()
    {
        $questionData = json_decode(
            file_get_contents(Config::DATA_DIR . '/questions_100.json'),
            true
        );

        $questionCount = count($questionData['questions']);
        $index = mt_rand(1, $questionCount) - 1;

        $this->question = QuestionFactory::make($questionData['questions'][$index], 100);

        $this->templateParameters['question'] = $this->question;
    }
}