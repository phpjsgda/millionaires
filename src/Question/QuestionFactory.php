<?php

namespace Sda\Millionaires\Question;

class QuestionFactory
{
    /**
     * @param array $questionData
     * @param int $value
     * @return Question
     * @throws IncorrectQuestionDataException
     */
    public static function make(array $questionData, $value)
    {
        if(false === array_key_exists('question', $questionData) ||
            false === array_key_exists('answers', $questionData)
        ){
            throw new IncorrectQuestionDataException('Brak wymaganych danych do zbudowania obiektu Question');
        }

        $answers = [];
        foreach ($questionData['answers'] as $answerData) {
            $answers[] = new Answer(
                $answerData['id'],
                $answerData['answer'],
                $answerData['correct']
            );
        }

        $question = new Question(
            $questionData['id'],
            $questionData['question'],
            $answers,
            $value
        );

        return $question;
    }
}