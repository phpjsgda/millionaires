<?php

namespace Sda\Millionaires\Question;

class Answer
{
    /**
     * @var string
     */
    private $text;
    /**
     * @var bool
     */
    private $isCorrect;
    /**
     * @var int
     */
    private $id;

    /**
     * Answer constructor.
     * @param int $id
     * @param string $text
     * @param bool $isCorrect
     */
    public function __construct($id, $text, $isCorrect)
    {
        $this->text = $text;
        $this->isCorrect = $isCorrect;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return bool
     */
    public function isCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}