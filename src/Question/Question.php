<?php

namespace Sda\Millionaires\Question;

class Question
{
    /**
     * @var string
     */
    private $question;
    /**
     * @var array
     */
    private $answers;
    /**
     * @var int
     */
    private $value;
    /**
     * @var int
     */
    private $id;

    /**
     * Question constructor.
     * @param int $id
     * @param string $question
     * @param array $answers
     * @param int $value
     */
    public function __construct($id, $question, array $answers, $value)
    {
        $this->question = $question;
        $this->answers = $answers;
        $this->value = $value;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @return array
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}