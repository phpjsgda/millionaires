<?php

use Sda\Millionaires\Config\Config;
use Sda\Millionaires\Controller\Millionaires;
use Sda\Millionaires\Request\Request;
use Sda\Millionaires\Session\Session;
use Sda\Millionaires\View\Template;

require_once __DIR__ . '/../vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(Config::TEMPLATE_DIR);
$twig = new Twig_Environment(
    $loader,
    [
        'cache' => false
    ]
);

$template = new Template($twig);
$request = new Request();
$session = new Session();

$app = new Millionaires(
    $template,
    $request,
    $session
);

$app->run();