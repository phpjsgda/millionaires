<?php

namespace Sda\Millionaires\Session;

class Session
{
    /**
     * Session constructor.
     */
    public function __construct()
    {
        session_start();
    }

    /**
     *
     */
    public function sessionDestroy()
    {
        session_destroy();
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function saveToSession($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * @param string $defaultValue
     * @return string
     */
    public function getFromSession($key, $defaultValue = '')
    {
        if (true === array_key_exists($key, $_SESSION) && '' !== $_SESSION[$key]) {
            return $_SESSION[$key];
        }
        return $defaultValue;
    }
}