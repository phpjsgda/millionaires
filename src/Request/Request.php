<?php

namespace Sda\Millionaires\Request;

class Request
{
    /**
     * @param string $key
     * @param string $defaultValue
     * @return mixed
     */
    public function getParamFromGet($key, $defaultValue = '')
    {
        return $this->getFromRequest($key, $defaultValue, $_GET);
    }

    /**
     * @param string $key
     * @param string $defaultValue
     * @return mixed
     */
    public function getParamFromPost($key, $defaultValue = '')
    {
        return $this->getFromRequest($key, $defaultValue, $_POST);
    }

    /**
     * @param string $key
     * @param string $defaultValue
     * @param array $source
     * @return mixed
     */
    private function getFromRequest($key, $defaultValue, $source)
    {
        if (true === array_key_exists($key, $source)){
            return $source[$key];
        }
        return $defaultValue;
    }
}