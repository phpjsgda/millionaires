<?php

namespace Sda\Millionaires\Config;

class Config
{
    const TEMPLATE_DIR = __DIR__ . '/../View/templates';
    const DATA_DIR = __DIR__ . '/../../data';

    const PRIZES =[
        1000000,
        500000,
        250000,
        125000,
        64000,
        32000,
        16000,
        8000,
        4000,
        2000,
        1000,
        500,
        300,
        200,
        100
    ];

}